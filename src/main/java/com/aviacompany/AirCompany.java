package com.aviacompany;

import com.aviacompany.plane.Plane;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class AirCompany {
    private final List<Plane> planeList = new ArrayList<>();

    public AirCompany(final Plane... planes) {
        planeList.addAll(Arrays.asList(planes));
    }

    public final int getTotalLoadCapacity() {
        return planeList.stream()
                .map(Plane::getWeightCapacity)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public final int getTotalCapacity() {
        return planeList.stream()
                .map(Plane::getNumberOfSits)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public final List<Plane> sortByMaxDistance() {
        planeList.sort(Comparator.comparingInt(Plane::getMaxDistance));
        return planeList;
    }

    public final void getPlaneList() {
        for (Plane pl : planeList) {
            System.out.println(pl.toString());
        }
    }

    public final void addPlanes(Plane... planes) {
        planeList.addAll(Arrays.asList(planes));
    }

    public final int getNumberOfPlanes() {
        return planeList.size();
    }


}
