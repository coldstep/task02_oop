package com.aviacompany.plane;

public enum WeightClass {
    //75 tons and higher
    FIRST,
    //from 30 to 75 tons
    SECOND,
    //from 10 to 30 tons
    THIRD,
    //to 10 tons
    FOURTH
}
