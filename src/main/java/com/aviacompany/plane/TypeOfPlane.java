package com.aviacompany.plane;


public enum TypeOfPlane {
    PASSENGER,
    TRANSPORT,
    POST,
    AGRICULTURAL,
    EDUCATIONAL,
    SPORTS,
    SPECIAL
}
