package com.aviacompany.plane.director;

import com.aviacompany.plane.Plane;
import com.aviacompany.plane.TypeOfPlane;

public class Director {
    public final Plane buildEmbraer190() {
        return new Plane(
                "Embraer 190",
                TypeOfPlane.PASSENGER,
                2013,
                104,
                51800,
                2,
                900,
                3300
        );
    }

    public final Plane buildBoeing737() {
        return new Plane(
                "Boeing-737",
                TypeOfPlane.PASSENGER,
                2015,
                186,
                79000,
                2,
                830,
                6000
        );
    }

    public final Plane buildBoeing767() {
        return new Plane(
                "Boeing-767",
                TypeOfPlane.PASSENGER,
                2018,
                261,
                186880,
                2,
                850,
                11070
        );
    }

    public final Plane buildBoeing777() {
        return new Plane(
                "Boeing-777",
                TypeOfPlane.PASSENGER,
                2018,
                361,
                297560,
                2,
                905,
                14260

        );
    }

    public final Plane buildRuslan() {
        return new Plane(
                "Ruslan AN-124",
                TypeOfPlane.TRANSPORT,
                2004,
                8,
                392000,
                4,
                865,
                7500
        );
    }


}
