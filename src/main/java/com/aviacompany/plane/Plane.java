package com.aviacompany.plane;

public class Plane {

    private final String name;

    private final int year;
    private final int numberOfSits;
    private final int weightCapacity;
    private final int numberOfEngines;
    private final int speed;
    private final int maxDistance;

    private WeightClass weightClass;
    private final TypeOfPlane type;

    public Plane(String name, TypeOfPlane type,
                 int year, int numberOfSits,
                 int weightCapacity, int numberOfEngines,
                 int speed, int maxDistance) {
        this.name = name;
        this.type = type;
        this.year = year;
        this.numberOfSits = numberOfSits;
        this.weightCapacity = weightCapacity;
        setWeightClass(weightCapacity);
        this.numberOfEngines = numberOfEngines;
        this.speed = speed;
        this.maxDistance = maxDistance;
    }

    public final String getName() {
        return name;
    }

    public final TypeOfPlane getType() {
        return type;
    }

    public final int getYear() {
        return year;
    }

    public final int getNumberOfSits() {
        return numberOfSits;
    }

    public final int getWeightCapacity() {
        return weightCapacity;
    }

    public final WeightClass getWeightClass() {
        return weightClass;
    }

    public final int getNumberOfEngines() {
        return numberOfEngines;
    }

    public final int getSpeed() {
        return speed;
    }

    public final int getMaxDistance() {
        return maxDistance;
    }

    private final void setWeightClass(int weightCapacity) {
        if (weightCapacity >= 75000) {
            weightClass = WeightClass.FIRST;
        } else if (weightCapacity > 30000 && weightCapacity < 75000) {
            weightClass = WeightClass.SECOND;
        } else if (weightCapacity > 10000 && weightCapacity < 30000) {
            weightClass = weightClass.THIRD;
        } else {
            weightClass = WeightClass.FOURTH;
        }
    }


    @Override
    public final String toString() {
        return "Plane " + name
                + " \nType = " + type
                + " \nYear = " + year
                + " \nNumber Of sits = " + numberOfSits
                + " \nWeigh capacity = " + weightCapacity
                + " \nWeight class = " + weightClass
                + " \nNumber Of engines = " + numberOfEngines
                + " \nSpeed = " + speed + "km/h"
                + " \nMax distance = " + maxDistance
                + "\n";
    }
}
