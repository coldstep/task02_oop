import com.aviacompany.AirCompany;
import com.aviacompany.plane.director.Director;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        AirCompany airCompany = new AirCompany(new Director().buildBoeing737(),
                new Director().buildBoeing767(),
                new Director().buildBoeing777(),
                new Director().buildEmbraer190(),
                new Director().buildRuslan());

        System.out.println("Total numbers of planes = " + airCompany.getNumberOfPlanes());
        System.out.println("Total numbers of sits from all planes = " + airCompany.getTotalCapacity());
        System.out.println("Total capacity from all planes = " + airCompany.getTotalLoadCapacity());
        System.out.println();
        airCompany.getPlaneList();
        System.out.println();
        System.out.println("After sort");
        System.out.println();
        List list = airCompany.sortByMaxDistance();
        for (Object obj : list) {
            System.out.println(obj.toString());
        }
        System.out.println();
        System.out.println("After sort ");
        System.out.println();
        airCompany.addPlanes(new Director().buildRuslan(), new Director().buildEmbraer190());
        airCompany.getPlaneList();


    }
}
